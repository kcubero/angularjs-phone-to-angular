import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {DashboardComponent} from './dashboard.component';
import {SettingsComponent} from './settings.component';

const routes: Routes = [
  {
    path: 'ng2-route',
    component: SettingsComponent
  },
  {
    path: 'ng2-route/dashboard',
    component: DashboardComponent
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'ng2-route'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    useHash: true,
    enableTracing: true
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
